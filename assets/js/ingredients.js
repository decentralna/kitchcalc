let ingredients = [
	{
	"name": "noodles",
	"quantity": 0.5,
	"unit": "packets",
	},
	{
	"name": "red onion",
	"quantity": 60,
	"unit": "grams",
	},
	{
	"name": "courgette",
	"quantity": 50,
	"unit": "grams",
	},
	{
	"name": "pepper",
	"quantity": 50,
	"unit": "grams",
	},
	{
	"name": "aubergine",
	"quantity": 70,
	"unit": "grams",
	},
	{
	"name": "spices",
	"quantity": 3,
	"unit": "grams",
	},
	{
	"name": "eggs",
	"quantity": 1.5,
	"unit": "",
	},
	{
	"name": "garlic",
	"quantity": 10,
	"unit": "cloves",
	},
];


