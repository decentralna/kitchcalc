#!/bin/sh

if [ ! -d node_modules ]; then
	git submodule themes/blist update --init
	cp themes/blist/package.json .
	sudo npm install
	sudo npm i -g postcss-cli
	sudo npm audit fix
else
	sudo npm update
	sudo npm audit fix
fi
