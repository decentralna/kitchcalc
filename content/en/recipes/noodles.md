---
author: "The Internet"
title: "Noodles"
date: 2022-10-26
description: "Gourmet Noodles"
tags: ["emoji"]
<!-- thumbnail: https://www.bigbasket.com/media/uploads/p/l/40201920-4_1-nongshim-shin-red-super-spicy-cup-noodles.jpg -->
---

# Gourmet Super Noodles

Noodles

{{< noodles >}}


## Preparation

1. Cut the onions, peppers, and aubergine into *thin* strips.
1. Cut the courgette and garlic up pretty much any way you want.
1. Fry everything, add spices.
1. Add water, with noodles.
1. Once the noodles are breaking up, add the eggs.
